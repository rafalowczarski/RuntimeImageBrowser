using Assets.Script;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RafalOwczarski.ImageBrowser
{
    public class ImagePreview : MonoBehaviour
    {
        [SerializeField] private TMP_Text loadTimeText;
        [SerializeField] private TMP_Text fileNameText;
        [SerializeField] private RawImage RawImage;

        public void RefreshPreview(LoadedFileData data)
        {
            loadTimeText.text = $"Load time : {data.LoadingTime}ms";
            fileNameText.text = $"File name : {data.FileName}";
            RawImage.texture = data.Texture;
        }
    }
}