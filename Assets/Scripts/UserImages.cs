﻿using RafalOwczarski.ImageBrowser;
using System.Collections;
using System.Diagnostics;
using System.IO;
using UnityEngine;

namespace Assets.Script
{
    public class UserImages : MonoBehaviour
    {

        [SerializeField] private PreviewsContainer container;

        private Stopwatch stopwatch = new Stopwatch();
        FileLoader fileLoader = new FileLoader();
        private string imagesDirectoryPath;

        private void Start()
        {
            imagesDirectoryPath = Application.streamingAssetsPath + "/Images";
            Refresh();
        }
        
        public void Refresh()
        {
            StopAllCoroutines();
            StartCoroutine(LoadImages());
        }
        public IEnumerator LoadImages()
        {
            container.Clear();

            var directoryInfo = new DirectoryInfo(imagesDirectoryPath);
            var files = directoryInfo.GetFiles("*.png");

            for (int i = 0; i < files.Length; i++)
            {
                yield return new WaitForEndOfFrame();

                stopwatch.Reset();
                stopwatch.Start();

                Texture2D texture = fileLoader.LoadImage(files[i].FullName);

                stopwatch.Stop();

                LoadedFileData data = new LoadedFileData(stopwatch.Elapsed.TotalMilliseconds, files[i].Name, texture);
                container.AddFile(data);
            }
        }
    }
}
