﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace RafalOwczarski.ImageBrowser
{
    public class PreviewsContainer : MonoBehaviour
    {
        [SerializeField] private ImagePreview PreviewPrefab;
        private List<ImagePreview> previews = new List<ImagePreview>();
        private void Awake()
        {
            previews = GetComponentsInChildren<ImagePreview>().ToList();
        }
        public void Clear()
        {
            for (int i = 0; i < previews.Count; i++)
            {
                previews[i].gameObject.SetActive(false);
            }
        }
        public void AddFile(LoadedFileData data)
        {

            ImagePreview preview = GetEmptyPreviewComponent();

            preview.RefreshPreview(data);
            preview.gameObject.SetActive(true);
        }

        private ImagePreview GetEmptyPreviewComponent()
        {
            ImagePreview emptyPreview = null;

            for(int i = 0;i<previews.Count;i++)
            {
                if (previews[i].isActiveAndEnabled == false)
                {
                    emptyPreview = previews[i];
                    break;
                }
            }

            if (emptyPreview is null)
            {
                emptyPreview = Instantiate(PreviewPrefab, this.transform).GetComponent<ImagePreview>();
                previews.Add(emptyPreview);
            }

            return emptyPreview;
        }
    }
}
