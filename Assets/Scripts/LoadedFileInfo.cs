﻿using UnityEngine;

namespace RafalOwczarski.ImageBrowser
{
    public struct LoadedFileData
    {
        public double LoadingTime { get; private set; }
        public string FileName { get; private set; }
        public Texture2D Texture { get; private set; }

        public LoadedFileData(double loadingTime, string fileName, Texture2D texture)
        {
            LoadingTime = loadingTime;
            FileName = fileName;
            Texture = texture;
        }
    }
}