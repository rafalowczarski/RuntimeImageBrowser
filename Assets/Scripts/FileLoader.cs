﻿using System.IO;
using UnityEngine;

namespace RafalOwczarski.ImageBrowser
{
    public class FileLoader
    {
        public Texture2D LoadImage(string filePath)
        {
            Texture2D texture = new Texture2D(1, 1);

            using (FileStream stream = new FileStream(filePath, FileMode.Open))
            {
                byte[] data = new byte[stream.Length];
                stream.Read(data, 0, (int)stream.Length);

                texture.LoadImage(data);
            }

            return texture;
        }
    }
}
